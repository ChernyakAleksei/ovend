﻿using System.Net.Sockets;
using System.Text;

namespace Ovend.Models.Sockets
{
    public class SocketState
    {
        // Client  socket.
        public Socket WorkSocket = null;

        // Size of receive Buffer.
        public const int BufferSize = 1024;

        // Receive Buffer.
        public byte[] Buffer = new byte[BufferSize];

        // Received data string.
        public StringBuilder Sb = new StringBuilder();
    }
}
