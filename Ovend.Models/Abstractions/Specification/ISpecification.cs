﻿using System;
using System.Linq.Expressions;

namespace Ovend.Models.Abstractions.Specification
{
    public interface ISpecification<T> where T : class
    {
        Expression<Func<T, bool>> SatisfiedBy();
    }
}


