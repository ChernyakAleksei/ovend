﻿using System;

namespace Ovend.Models.Accessory
{
    public class OperationResult<T>
    {
        public T Data { get; set; }
        public bool IsSuccess { get; set; }
        public string EventName { get; set; }
        public Exception Exception { get; set; }


        public OperationResult() {
            IsSuccess = true;
        }


        public OperationResult(T data) {
            IsSuccess = true;
            Data = data;
        }


        public OperationResult(string eventName, Exception exception) {
            EventName = eventName;
            IsSuccess = false;
            Exception = exception;
        }
    }
}
