﻿using System;

namespace Ovend.Models.Events
{
    public class TcpClientEventAgrs : EventArgs
    {
        public readonly string EvaDtsData;

        public TcpClientEventAgrs(string evaDtsData)
        {
            EvaDtsData = evaDtsData;
        }
    }
}
