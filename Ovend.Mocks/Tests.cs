﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Ovend.Mocks
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void SerializeEvaDts()
        {
            var fileData = File.ReadAllText("d:\\EVADTS.txt");
            var evaSerializer = new EvaDts.Serializer();
            var evaDts = evaSerializer.Deserialize(fileData);
            var result = evaSerializer.Serialize(evaDts);
        }


        [Test]
        public void SendEvaDtsDataToServer() {
            try {
                var dataToSend = File.ReadAllText("h:\\EVADTS.txt");
                //for (var i = 0; i < 150; i++) {
                //    var j = i;
                //    Task.Run(() => { SendStringViaTcp(dataToSend + j); });
                //}
                Task.Run(() => SendStringViaTcp(dataToSend));
            }
            catch (Exception e) {
                var ex = e;
            }
        }


        private static void SendStringViaTcp(string dataToSend) {
            var ip = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 3320);
            var server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            server.Connect(ip);
            server.Send(Encoding.ASCII.GetBytes(dataToSend));
            var data = new byte[1024];
            var receivedDataLength = server.Send(data);
            var response = Encoding.ASCII.GetString(data, 0, receivedDataLength);

            server.Shutdown(SocketShutdown.Both);
            server.Close();
        }
    }
}
