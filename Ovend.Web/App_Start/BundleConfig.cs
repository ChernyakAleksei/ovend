﻿using System.Web;
using System.Web.Optimization;

namespace Ovend.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/a2styles")
                .Include("~/Content/styles.bundle.css"));


            bundles.Add(new ScriptBundle("~/bundles/a2scripts")
                .Include("~/Scripts/inline.bundle.js")
                .Include("~/Scripts/polyfills.bundle.js")
                .Include("~/Scripts/vendor.bundle.js")
                .Include("~/Scripts/main.bundle.js")
            );
        }
    }
}
