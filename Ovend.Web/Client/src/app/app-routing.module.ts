import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';

const routes: Routes = [
  // { path: 'about', loadChildren: 'app/about/about.module#AboutModule' }, 
  { 
    path: 'home', 
    component: HomeComponent, 
    // resolve: {
    //   singleImg: DataResolve
    // }
  },
  { path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports:    [RouterModule.forRoot(routes,{ useHash: true })],
  exports:    [RouterModule],
  providers:  []
})
export class AppRoutingModule {}