﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ovend.Web.Startup))]
namespace Ovend.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
