﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ovend.Sockets.AsynchronousSocket;

namespace Ovend.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public MvcApplication() {
            var sPort = System.Configuration.ConfigurationManager.AppSettings["TcpPort"];
            var iPort = Convert.ToInt32(sPort);
            _socketListener = new AsynchronousSocketListener(iPort);
        }


        private readonly AsynchronousSocketListener _socketListener;


        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Task.Run(() => _socketListener.Start());
        }


        protected void Application_End() {
            //Task.Run(() => _socketListener.End());
        }
    }
}
