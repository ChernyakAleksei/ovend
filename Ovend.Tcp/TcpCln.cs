﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Ovend.Tcp
{
    internal class TcpCln
    {
        private readonly TcpClient _client;

        /// <summary>
        /// Ovend Tcp Client
        /// Constructor: Transfer received from the client TcpListener
        /// </summary>
        /// <param name="client"></param>
        public TcpCln(TcpClient client) {
            _client = client;
        }


        public string GetEvaDtsData() {
            var evaDtsData = "";
            var buffer = new byte[8192];
            int count;

            // Read from the stream as long as the data is received from client
            while ((count = _client.GetStream().Read(buffer, 0, buffer.Length)) > 0) {
                evaDtsData += Encoding.ASCII.GetString(buffer, 0, count);
                if (evaDtsData.IndexOf("\r\n\r\n", StringComparison.Ordinal) >= 0 || evaDtsData.Length > buffer.Length) {
                    break;
                }
            }
            return evaDtsData;
        }
    }
}
