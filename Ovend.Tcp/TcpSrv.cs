﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using Ovend.Models.Events;

namespace Ovend.Tcp
{
    /// <summary>
    /// TCP Server for receiving Eva-dts data
    /// </summary>
    internal class TcpSrv : IDisposable
    {
        public delegate void TcpClientEventEventHandler(object sender, TcpClientEventAgrs args);
        public event TcpClientEventEventHandler ClientSendData;

        /// Invocators
        protected virtual void OnClientSendData(TcpClientEventAgrs args) {
            ClientSendData?.Invoke(this, args);
        }


        private TcpListener _tcpListener;


        public TcpSrv(IPAddress ip, int port) {
            var iPEndPoint = new IPEndPoint(ip, port);
            _tcpListener = new TcpListener(iPEndPoint);
        }


        public void Start() {
            _tcpListener.Start();
            while (true) {
                _tcpListener.AcceptTcpClient();
                ThreadPool.QueueUserWorkItem(ClientThread, _tcpListener.AcceptTcpClient());
            }
        }


        /// <summary>
        /// Create a new class Client instance and pass it to the converted class TcpClient object StateInfo
        /// </summary>
        /// <param name="stateInfo"></param>
        private void ClientThread(object stateInfo) {
            var tcpCln = new TcpCln((TcpClient)stateInfo);
            var evaDtsData = tcpCln.GetEvaDtsData();
            OnClientSendData(new TcpClientEventAgrs(evaDtsData));
        }


        public void Dispose() {
            _tcpListener.Stop();
            _tcpListener = null;
        }

    }
}
