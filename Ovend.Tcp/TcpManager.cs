﻿using System;
using System.Net;
using System.Threading;
using Ovend.Models.Events;

namespace Ovend.Tcp
{
    public class TcpManager
    {
        private readonly TcpSrv _tcpServer;

        private int counter;

        public TcpManager(IPAddress ip, int port) {
            // Set the maximum number of worker threads
            var maxThreadsCount = Environment.ProcessorCount*4;

            ThreadPool.SetMaxThreads(maxThreadsCount, maxThreadsCount);

            // Set the minimum number of worker threads
            ThreadPool.SetMinThreads(2, 2);

            // Create new server
            _tcpServer = new TcpSrv(ip, port);
            _tcpServer.ClientSendData += TcpServerOnClientSendData;
        }

        private void TcpServerOnClientSendData(object sender, TcpClientEventAgrs args) {
            counter++;
        }


        public void Start() {
            _tcpServer.Start();
        }
    }
}
