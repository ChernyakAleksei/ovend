﻿using System;

namespace Ovend.EvaDts.Accessory
{
    public class OperationResult<T>
    {
        public T Data { get; set; }
        public bool IsSuccess { get; set; }
        public string EventName { get; set; }
        public string UnknownType { get; set; }
        public Exception Exception { get; set; }


        public OperationResult() {
            IsSuccess = true;
        }


        public OperationResult(T data) {
            IsSuccess = true;
            Data = data;
        }


        public OperationResult(string eventName, Exception exception, string unknownType) {
            IsSuccess = false;
            EventName = eventName;
            Exception = exception;
            UnknownType = unknownType;
        }
    }
}
