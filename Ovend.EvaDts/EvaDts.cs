﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Enums;
using Ovend.EvaDts.Models.Parameters.Footer;
using Ovend.EvaDts.Models.Parameters.Header;
using Ovend.EvaDts.Models.Wrappers;
using Ovend.EvaDts.Virtualization.Abstractions.Groups;

namespace Ovend.EvaDts
{
    public class EvaDts
    {
        // Header
        public Dxs Dxs { get; set; }
        public St St { get; set; }

        // Footer
        public Dxe Dxe { get; set; }
        public Se Se { get; set; }

        // Parameters
        public List<Ea> Ea { get; set; }
        public List<Id> Id { get; set; }
        public List<Ic> Ic { get; set; }
        public List<Pa> Pa { get; set; }
        public List<Sd> Sd { get; set; }


        // Accesory
        public bool ChecksumIsValid { get; set; }
        public RequestType RequestType { get; set; }
        public List<string> UnknownTypes { get; set; }
        public bool IsSuccess { get; set; }
        public List<Exception> Exception { get; set; }


        // Parameterless constructor
        public EvaDts() {
            IsSuccess = true;
            Exception = new List<Exception>();
            UnknownTypes = new List<string>();
        }


        // Deserialization constructor
        public EvaDts(SerializationInfo info) {
            IsSuccess = true;
            Exception = new List<Exception>();
            UnknownTypes = new List<string>();
            foreach (var entry in info) {
                try {
                    var parsedLine = new ParsedLine(entry.Name, info);
                    var setResult = new Setable().Set(this, parsedLine);
                    if (setResult.IsSuccess) continue;
                    IsSuccess = false;
                    Exception.Add(setResult.Exception);
                    UnknownTypes.Add(setResult.UnknownType);
                }
                catch (Exception ex) {
                    IsSuccess = false;
                    Exception.Add(ex);
                }
            }
            RequestType = DetermineRequestType();
            ChecksumIsValid = CheckSumValidation();
        }


        private RequestType DetermineRequestType() {
            if (Id.Any()) {
                return RequestType.Registration;
            }
            return RequestType.DataTransfer;
        }


        private bool CheckSumValidation() {
            if (!Sd.Any()) return false;
            var sd = Sd.First();
            if (sd.GetType() == typeof(Models.Parameters.Body.Sd.Sd1)) {
                var checkSum = ((Models.Parameters.Body.Sd.Sd1)sd).CheckSum;
                return checkSum == GetCheckSum();
            }
            return false;
        }


        private string GetCheckSum() {
            return "0";
        }
    }
}
