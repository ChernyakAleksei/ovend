﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ovend.EvaDts.Models.Wrappers;
using Ovend.EvaDts.Virtualization.Abstractions;
using Ovend.EvaDts.Virtualization.Interfaces;

namespace Ovend.EvaDts.Implementation
{
    public class Setable : ISetable
    {
        /// <summary>
        /// Set impl. property by Name
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="parsedLine"></param>
        /// <returns></returns>
        public Accessory.OperationResult<bool> Set(object obj, ParsedLine parsedLine) {
            var isGroup = !string.IsNullOrEmpty(parsedLine.GroupName);
            var type = obj.GetType();
            var propInfo = type.GetProperty(isGroup ? parsedLine.GroupName : parsedLine.Name);
            if (propInfo == null) return new Accessory.OperationResult<bool>("[Setable/Set]", new Exception("Unknown parameter: " + parsedLine.Name), parsedLine.Name);

            if (isGroup) {
                if (propInfo.PropertyType.Name == typeof(List<>).Name) {
                    if (propInfo.PropertyType.GetGenericArguments()[0].IsSubclassOf(typeof(Parameter))) {
                        var target = propInfo.GetValue(obj);
                        if (target == null) {
                            target = Activator.CreateInstance(propInfo.PropertyType);
                            propInfo.SetValue(obj, target, null);                        
                        }
                        var instanceType = GetTypeByName(parsedLine.Name);
                        if (instanceType != null && instanceType != typeof(object)) {
                            var instance = Activator.CreateInstance(instanceType, parsedLine);
                            propInfo.PropertyType.GetMethod("Add").Invoke(target, new[] { instance });
                        }
                        else {
                            return new Accessory.OperationResult<bool>("[Setable/Set]", new Exception("Unknown parameter: " + parsedLine.Name), parsedLine.Name);
                        }
                    }
                    else {
                        return new Accessory.OperationResult<bool>("[Setable/Set]", new Exception("Unknown parameter: " + parsedLine.Name), parsedLine.Name);
                    }
                }
                else {
                    return new Accessory.OperationResult<bool>("[Setable/Set]", new Exception("Unknown parameter: " + parsedLine.Name), parsedLine.Name);
                }
            }
            else {
                if (propInfo.PropertyType.IsSubclassOf(typeof(Parameter))) {
                    if ((type.GetProperty(propInfo.PropertyType.Name)).GetValue(obj) != null) return new Accessory.OperationResult<bool>();
                    var propObjValue = Activator.CreateInstance(propInfo.PropertyType, parsedLine);
                    propInfo.SetValue(obj, propObjValue, null);
                }
                else {
                    return new Accessory.OperationResult<bool>("[Setable/Set]", new Exception("Unknown parameter: " + parsedLine.Name), parsedLine.Name);
                }
            }
            return new Accessory.OperationResult<bool>();
        }


        private static Type GetTypeByName(string typeName) {
            Type instanceType = new TypeDelegator(typeof(object));
            foreach (var i4 in Assembly.GetExecutingAssembly().GetTypes().Where(i4 => i4.Name == typeName)) {
                instanceType = i4;
            }
            return instanceType;
        }
    }
}
