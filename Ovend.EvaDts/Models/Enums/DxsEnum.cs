﻿namespace Ovend.EvaDts.Models.Enums
{
    /// <summary>
    /// Va - Automat report
    /// Vc - Storage device report
    /// Vt - Terminal report
    /// </summary>
    public enum DxsEnum
    {
        Va,
        Vc,
        Vt
    }
}
