﻿using Ovend.EvaDts.Models.Wrappers;
using Ovend.EvaDts.Virtualization.Abstractions;

namespace Ovend.EvaDts.Models.Parameters.Footer
{
    /// Transaction footer
    public class Dxe : Parameter
    {
        public string Value1 { get; private set; }
        public string Value2 { get; private set; }

        public Dxe(ParsedLine line) : base(line) {
            Value1 = line.Values[0];
            Value2 = line.Values[1];
        }
    }
}
