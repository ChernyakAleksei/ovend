﻿using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Models.Parameters.Body.Ic
{
    /// <summary>
    /// Product information
    /// ABC7896*Разлив молока*01.00*Гв.Широнинцев*20170112*MILK-2017
    /// </summary>
    public class Ic1 : Virtualization.Abstractions.Groups.Ic
    {
        public string Id { get; set; } // Field number 1
        public string Type { get; set; } // Field number 2
        public string Rate { get; set; } // Field number 3
        public string Address { get; set; } // Field number 3
        public string Date { get; set; } // Field number 3
        public string Code { get; set; } // Field number 3

        public Ic1(ParsedLine line) : base(line) {
            new Setable().Set(this, line);
        }
    }
}
