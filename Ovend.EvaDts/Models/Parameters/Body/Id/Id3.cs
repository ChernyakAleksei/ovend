﻿using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Models.Parameters.Body.Id
{
    /// <summary>
    /// Product information
    /// </summary>
    public class Id3 : Virtualization.Abstractions.Groups.Id
    {
        public string Id { get; set; } // Field number 1
        public string Cost { get; set; } // Field number 2
        public string Name { get; set; } // Field number 3
        public string Value { get; set; } // Field number 6
        public string Status { get; set; } // Field number 7
        public string LevelCur { get; set; } // Field number 8
        public string LevelMin { get; set; } // Field number 9

        public Id3(ParsedLine line) : base(line) {
            new Setable().Set(this, line);
        }
    }
}
