﻿using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Models.Parameters.Body.Ea
{
    /// <summary>
    /// Product information
    /// </summary>
    public class Ea1 : Virtualization.Abstractions.Groups.Ea
    {
        public string Obw { get; set; } // Field number 1
        public string Id1 { get; set; } // Field number 2
        public string Id2 { get; set; } // Field number 3

        public Ea1(ParsedLine line) : base(line) {
            new Setable().Set(this, line);
        }
    }
}
