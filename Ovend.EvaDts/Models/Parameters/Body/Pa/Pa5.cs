﻿using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Models.Parameters.Body.Pa
{
    /// <summary>
    /// Product information
    /// </summary>
    public class Pa5 : Virtualization.Abstractions.Groups.Pa
    {
        public string Quantity { get; set; } // Field number 1,3
        public string Summ { get; set; } // Field number 2,4

        public Pa5(ParsedLine line) : base(line)
        {
            new Setable().Set(this, line);
        }

    }
}
