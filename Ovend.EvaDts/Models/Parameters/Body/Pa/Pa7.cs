﻿using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Models.Parameters.Body.Pa
{
    /// <summary>
    /// Additional product information
    /// </summary>
    public class Pa7 : Virtualization.Abstractions.Groups.Pa
    {
        public string Id { get; set; } // Field number 1
        public string NdsGroup { get; set; } // Field number 3
        public string SaleStatus { get; set; } // Field number 4
        public string PaymentStatus { get; set; } // Field number 5

        public Pa7(ParsedLine line) : base(line) {
            new Setable().Set(this, line);
        }
    }
}
