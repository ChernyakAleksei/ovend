﻿using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;
using Ovend.EvaDts.Virtualization.Abstractions;

namespace Ovend.EvaDts.Models.Parameters.Body.Pa
{
    /// <summary>
    /// Additional product information
    /// </summary>
    public class Pa6 : Virtualization.Abstractions.Groups.Pa
    {
        public string Id { get; set; } // Field number 1
        public string NdsGroup { get; set; } // Field number 3
        public string SaleStatus { get; set; } // Field number 4
        public string PaymentStatus { get; set; } // Field number 5

        public Pa6(ParsedLine line) : base(line) {
            new Setable().Set(this, line);
        }
    }
}


