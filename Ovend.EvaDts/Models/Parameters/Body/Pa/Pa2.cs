﻿using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Models.Parameters.Body.Pa
{
    /// <summary>
    /// Selling goods
    /// </summary>
    public class Pa2 : Virtualization.Abstractions.Groups.Pa
    {
        public string Count { get; set; } // Field number 1,3
        public string Summ { get; set; } // Field number 2,4
        public string CountWithDiscount { get; set; } // Field number 5,7
        public string SummWithDiscount { get; set; } // Field number 6,8
        public string CountWithSurcharge { get; set; } // Field number 9,11
        public string SummWithSurcharge { get; set; } // Field number 12,13

        public Pa2(ParsedLine line) : base(line) {
            new Setable().Set(this, line);
        }
    }
}
