﻿using System.Linq;
using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Models.Parameters.Body.Sd
{
    /// <summary>
    /// Product information
    /// </summary>
    public class Sd1 : Virtualization.Abstractions.Groups.Sd
    {
        public string CheckSum { get; set; } // Field number 1

        public Sd1(ParsedLine line) : base(line) {
            new Setable().Set(this, line);
            if (line != null && line.Values.Any()) {
                CheckSum = line.Values[0];
            }
        }
    }
}
