﻿using Ovend.EvaDts.Models.Enums;
using Ovend.EvaDts.Models.Wrappers;
using Ovend.EvaDts.Virtualization.Abstractions;

namespace Ovend.EvaDts.Models.Parameters.Header
{
    public class Dxs : Parameter {

        /// <summary>
        /// Va - Automat report
        /// Vc - Storage device report
        /// Vt - Terminal report
        /// </summary>
        public DxsEnum ReportType { get; private set; }

        /// <summary>
        /// Protocol version
        /// </summary>
        public string Version { get; private set; }


        public Dxs(ParsedLine line) : base(line) {
            ReportType = DxsEnum.Va;
            Version = "1.1";
        }
    }
}
