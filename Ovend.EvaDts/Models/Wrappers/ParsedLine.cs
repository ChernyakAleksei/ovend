﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;

namespace Ovend.EvaDts.Models.Wrappers
{
    public class ParsedLine
    {
        public string Name { get; set; }
        public string GroupName { get; set; }
        public string Row { get; set; }
        public List<string> Values { get; set; }


        public ParsedLine(string entryName, SerializationInfo info) {
            Name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(entryName.ToLower());
            GroupName = Name.ToCharArray().Any(char.IsDigit) ? new String(Name.Where(c => c != '-' && (c < '0' || c > '9')).ToArray()) : null;
            Row = info.GetValue(entryName, typeof(string)).ToString();
            Values = GetValues();
        }


        private List<string> GetValues() {
            var sublines = Row.Split('*');
            return sublines.Where((t, i) => i != 0).ToList();
        }
    }
}
