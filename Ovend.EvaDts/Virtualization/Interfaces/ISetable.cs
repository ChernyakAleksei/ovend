﻿using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Virtualization.Interfaces
{
    interface ISetable
    {
        Accessory.OperationResult<bool> Set(object obj, ParsedLine line);
    }
}
