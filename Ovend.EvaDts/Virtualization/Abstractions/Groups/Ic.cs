﻿using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Virtualization.Abstractions.Groups
{
    public abstract class Ic : Parameter
    {
        protected Ic(ParsedLine line) : base(line) {
            new Setable().Set(this, line);
        }
    }
}
