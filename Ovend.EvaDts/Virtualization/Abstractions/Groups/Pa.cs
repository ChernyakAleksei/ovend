﻿using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Virtualization.Abstractions.Groups
{
    public abstract class Pa : Parameter
    {
        protected Pa(ParsedLine line) : base(line) {
            new Setable().Set(this, line);
        }
    }
}
