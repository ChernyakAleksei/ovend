﻿using Ovend.EvaDts.Implementation;
using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Virtualization.Abstractions.Groups
{
    public abstract class Id : Parameter
    {
        protected Id(ParsedLine line) : base(line) {
            new Setable().Set(this, line);
        }
    }
}
