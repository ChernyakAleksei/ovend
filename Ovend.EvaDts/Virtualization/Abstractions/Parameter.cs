﻿using Ovend.EvaDts.Models.Wrappers;

namespace Ovend.EvaDts.Virtualization.Abstractions
{
    public abstract class Parameter
    {
        public ParsedLine Line;

        /// <summary>
        /// Set restraint the parameterless constructor
        /// </summary>
        /// <param name="line"></param>
        protected Parameter(ParsedLine line) {
            Line = line;
        }

    }
}
