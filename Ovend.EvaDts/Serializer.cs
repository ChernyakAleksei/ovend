﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Ovend.EvaDts
{
    public class Serializer {
        /// <summary>
        /// Eva-Dts Serialize
        /// </summary>
        /// <param name="eva"></param>
        /// <returns></returns>
        public string Serialize(EvaDts eva) {
            var sb = new System.Text.StringBuilder();
            // Header
            sb.AppendLine(eva.Dxs.Line.Row);
            sb.AppendLine(eva.St.Line.Row);
            // Body
            if (eva.UnknownTypes.Any()) {
                foreach (var unknownType in eva.UnknownTypes) {
                    sb.AppendLine(unknownType.ToUpper());
                }
            }
            // Footer
            sb.AppendLine(eva.Se.Line.Row);
            sb.AppendLine(eva.Dxe.Line.Row);
            return sb.ToString();
        }


        /// <summary>
        /// Eva-Dts Deserialize
        /// </summary>
        /// <param name="filedata"></param>
        /// <returns></returns>
        public EvaDts Deserialize(string filedata) {
            try {
                var deserializationInfo = new SerializationInfo(typeof(string), new FormatterConverter());
                foreach (var line in filedata.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None)) {
                    if (string.IsNullOrEmpty(line)) continue;
                    deserializationInfo.AddValue(line.Substring(0, line.IndexOf('*')), line);
                }
                var evadts = new EvaDts(deserializationInfo);
                return evadts;
            }
            catch (Exception ex) {
                var evaDts = new EvaDts {IsSuccess = false};
                evaDts.Exception.Add(ex);
                return evaDts;
            }
        }
    }
}
