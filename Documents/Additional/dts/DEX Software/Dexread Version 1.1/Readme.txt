I found it necessary to create my own Borland project file in order to compile the DexRead
program.  In order to use the 16-bit debugger (Turbo Debugger), I found it necessary to 
rename Dex_audit.cpp to Dex_aud.cpp.   It compiles fine with either, but the debugger requires DOS filenames.   This also requires the project to call out the different module.

It was decided to package the source code without the name change since the documentation 
refers to the Dex_Audit.CPP by name and this module may be referenced by other software.

-KEK 8/29/02