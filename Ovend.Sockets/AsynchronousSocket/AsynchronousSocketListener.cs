﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Ovend.Models.Sockets;

namespace Ovend.Sockets.AsynchronousSocket
{
    public class AsynchronousSocketListener
    {
        // Thread signal.
        public static ManualResetEvent AllDone = new ManualResetEvent(false);

        private readonly IPEndPoint _localEndPoint;
        private Socket _listener;


        public AsynchronousSocketListener(int port) {
            _localEndPoint = new IPEndPoint(IPAddress.Any, port);

            // Create a TCP/IP socket.
            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }


        public void Start() {
            try {
                // Bind the socket to the local endpoint and listen for incoming connections.
                _listener.Bind(_localEndPoint);
                _listener.Listen(100);

                while (true) {
                    // Set the event to nonsignaled state.
                    AllDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    _listener.BeginAccept(AcceptCallback, _listener);

                    // Wait until a connection is made before continuing.
                    AllDone.WaitOne();
                }
            }
            catch (SocketException sex) {
                Restart();
            }
            catch (Exception ex) {
                Restart();
            }
        }


        public void End() {
            try {
                if (_listener != null) {
                    _listener.Dispose();
                }
            }
            catch (SocketException sex) {
            }
            catch (Exception ex) {
            }
        }


        public void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.
            AllDone.Set();

            // Get the socket that handles the client request.
            var listener = (Socket)ar.AsyncState;
            var handler = listener.EndAccept(ar);

            // Create the state object.
            var state = new SocketState {
                WorkSocket = handler
            };
            handler.BeginReceive(state.Buffer, 0, SocketState.BufferSize, 0, ReadCallback, state);
        }


        public void ReadCallback(IAsyncResult ar)
        {
            // Retrieve the state object and the handler socket
            // from the asynchronous state object.
            var state = (SocketState)ar.AsyncState;
            var handler = state.WorkSocket;

            // Read data from the client socket. 
            var bytesRead = handler.EndReceive(ar);

            if (bytesRead <= 0) return;

            // There  might be more data, so store the data received so far.
            state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));

            // Check for end-of-file tag. If it is not there, read 
            // more data.
            var content = state.Sb.ToString();

            // Echo the data back to the client.
            Send(handler, content);

            //if (content.IndexOf("<EOF>") > -1)
            //{
            //    // Echo the data back to the client.
            //    Send(handler, content);
            //}
            //else {
            //    // Not all data received. Get more.
            //    handler.BeginReceive(state.Buffer, 0, SocketState.BufferSize, 0, ReadCallback, state);
            //}
        }


        private void Send(Socket handler, string data) {
            try {
                // Convert the string data to byte data using ASCII encoding.
                var byteData = Encoding.ASCII.GetBytes(data);

                // Begin sending the data to the remote device.
                handler.BeginSend(byteData, 0, byteData.Length, 0, SendCallback, handler);
            }
            catch (Exception ex) {
                Restart();
            }
        }


        private void SendCallback(IAsyncResult ar) {
            try {
                // Retrieve the socket from the state object.
                var handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                var bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();

            }
            catch (Exception ex) {
                Restart();
            }
        }


        private void Restart() {
            _listener.Dispose();
            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Start();
        }
    }
}
