﻿using System.Web.Hosting;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.Practices.ServiceLocation;
using Ovend.Logger;
using Ovend.Models.Abstractions.Accessory;
using Ovend.Models.Abstractions.Contracts;
using Ovend.Models.Abstractions.UnitOfWork;
using Ovend.Orm.Unitofwork;

namespace Ovend.Localisation
{
    public class WebEntityInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store) {

            container.Register(Component.For(typeof(IRepository<>)).ImplementedBy(typeof(Repository<>)).LifestyleTransient());
            container.Register(Component.For<IQueryableUnitOfWork, UnitOfWork>().ImplementedBy<UnitOfWork>().LifestyleTransient());

            // container.Register(Component.For<IUsers>().ImplementedBy<ImpUser>().LifestyleTransient());

            container.Register(Component.For<ILog>().ImplementedBy<LogManager>().LifestyleTransient().
                                         DependsOn(Dependency.OnValue("logFolder", HostingEnvironment.ApplicationPhysicalPath + @"\Logs")));

            var controllerFactory = new WindsorControllerFactory(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
            ServiceLocator.SetLocatorProvider(() => new WindsorServiceLocator(container));
        }
    }
}