﻿using System;
using System.Collections.Generic;
using System.Web.Http.Controllers;
using Ovend.Models.Abstractions.Factory;

namespace Ovend.Localisation
{
    public class ReleasingControllerFactory : IHttpControllerFactory
    {
        public IHttpController CreateController(HttpControllerContext controllerContext, String controllerName)
        {
            return new HttpControllerContext() as IHttpController;
        }

        public void ReleaseController(HttpControllerContext controllerContext, IHttpController controller)
        {
        }

        public IDictionary<string, HttpControllerDescriptor> GetControllerMapping()
        {
            return new Dictionary<string, HttpControllerDescriptor>();
        }
    }
}